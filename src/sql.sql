DROP DATABASE IF EXISTS test;
CREATE DATABASE `test`;
USE `test`;

  CREATE TABLE message (
    id   INT         NOT NULL AUTO_INCREMENT,
    text VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
    ) ENGINE = INNODB DEFAULT CHARSET = utf8;	
  INSERT INTO `message` (`text`) VALUES ('sysadmin');
  INSERT INTO `message` (`text`) VALUES ('sysad1min');
  INSERT INTO `message` (`text`) VALUES ('sysad11min');
select * from `message`