package thumbtack.hospital.snik2004.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import thumbtack.hospital.snik2004.domain.Message;
import thumbtack.hospital.snik2004.repo.MessageRepo;

@RestController
@RequestMapping("/message")
public class MessageController {

    private final MessageRepo messageRepo;

    @Autowired
    public MessageController(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }


//    @GetMapping
//    public long list() {
//        System.out.println("ololo");
//        return messageRepo.count();
//    }

    @GetMapping(value = "/{id}", produces     = MediaType.TEXT_PLAIN_VALUE)
    public Message getAdmin(@RequestParam("id") int id) {
        System.out.println("ok");
        Message message = messageRepo.getOne(Long.valueOf(id));
        System.out.println(message.getId());
        return message;
    }

    @PostMapping
    public Message create(@RequestBody Message message) {
        System.out.println("olololo");
        messageRepo.save(message);
        messageRepo.flush();
        return message;
    }

    @PutMapping
    public Message update(
            @PathVariable("id") Message messageFromDB,
            @RequestBody Message message) {

        BeanUtils.copyProperties(message, messageFromDB);
        return messageRepo.save(message);
    }

    @DeleteMapping({"id"})
    public void delete(@PathVariable("id") Message message) {
        messageRepo.delete(message);

    }
}

