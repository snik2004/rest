package thumbtack.hospital.snik2004.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import thumbtack.hospital.snik2004.domain.Message;

public interface MessageRepo extends JpaRepository <Message,Long> {


}
